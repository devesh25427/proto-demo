# cloudproject

Demo project to showcase protobuf plugins.

api/cp/v2/cp.proto declares protocol buffer messages and GRPC service.

Plugins used:
protoc-gen-swagger (https://github.com/grpc-ecosystem/grpc-gateway ) 

protoc-gen-grpc-gateway (https://github.com/grpc-ecosystem/grpc-gateway )

protoc-gen-gorm (https://github.com/infobloxopen/protoc-gen-gorm)

protc-gen- govalidators (https://github.com/mwitkow/go-proto-validators )

All pugin are configured in prototool.yaml https://github.com/uber/prototool. protoc compiler is invoked using prototool.

Compilation:
run 'make build-cli'


To generate code from proto file inside docker use
'make docker-code-gen'

Code Struction
api/cp/v2 : contains api definition. All files except _cp.proto_ are auto generated using protoc compiler
- cp.proto : This file has protobuf and gRPC definition.
- cp.swagger.json : Swagger file for REST APIs
- cp.pb.gorm.go : GO ORM bindings
- cp.validator.gw.go : validation code generation.

cms/cp : Contain main.go for the application.
pkg/cpserver : microservice application logic

Testing :
Assuming serive is running on localhost port 8080
PUT call
http://127.0.0.1:8080/v2/info?value=10
BODY:
{
        "infoId": "123",
        "description": "ss",
        "value": 10
}

Get call
http://127.0.0.1:8080/v2/info?value=10

Another tool like prototool (buf)
https://github.com/bufbuild/buf
https://github.com/johanbrandhorst/grpc-gateway-boilerplate 


    

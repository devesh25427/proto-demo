.PHONY: all build gen-go-proto gen ui
CURDIR:=${PWD}
EXE=
ifeq ($(OS), Windows_NT)
	EXE=.exe
endif
SWAGGERHOSTNAME=cp.devesh25427.io

#For cloudrun
DEFAULTENDPOINT=cpdefault.endpoints.myproject.cloudrun.cloud.goog
export FULLSERVICEDOMAIN ?= $(FULLDOMAIN)
#generate code inside docker
docker-code-gen:
		docker build -t cp_gen -f Dockerfile.gen .
		rm -rf generated/*
		docker run -v ${CURDIR}/generated/:/cloudproject/generated cp_gen


gen:  gen-go-proto
		@rm -rf ./generated


gen-go-proto:
		@mkdir -p ./generated
		@prototool generate || true
		@cp -r ./generated/gitlab.com/devesh25427/cloudproject/apis/cp/v2/* ./generated/cp/v2/

coverage:
		mkdir -p build
		go test -race -v -coverprofile build/coverage.out ./...
		go tool cover -html=build/coverage.out -o build/coverage.html

##Make local docker image
docker-cp:
	docker build -t cloudproject --target run .
#Run service
docker-cp-run:
	docker run -it -p 8080:8080 cloudproject

#CLI
build-cli:
	go build -o cmd/cp/cp$(EXE) cmd/cp/main.go

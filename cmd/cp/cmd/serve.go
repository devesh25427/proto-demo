/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"strconv"

	"github.com/go-sql-driver/mysql"
	grpc_validation "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/devesh25427/cloudproject/apis/cp/v2"
	"gitlab.com/devesh25427/cloudproject/pkg/cpserver"
	"google.golang.org/grpc"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		return serve()
	},
}

const (
	defaultPort = 8080
)

var (
	payshieldFQDN      string
	projectId          string
	debug              bool
	host               string
	clientCert         string
	clientCertPassword string
	port               int
	listGrpc           net.Listener
	sbServer           *cpserver.Server
	dbCheck            bool
	dbCheckTimes       int
	dbDialect, dbConn  string
)

var mysqlConfig = &mysql.Config{
	User:                 "root",
	Net:                  "tcp",
	Addr:                 "cp-db",
	DBName:               "cp",
	AllowNativePasswords: true,
}

func init() {
	rootCmd.AddCommand(serveCmd)

	serveCmd.Flags().BoolVar(&dbCheck, "dbcheck", true, "check and wait for db to be active then exit (for init container)")
	serveCmd.Flags().IntVar(&dbCheckTimes, "dbcheck-retries", 20, "Number of times to retry connecting to the DB server.")
	serveCmd.Flags().StringVar(&payshieldFQDN, "ps-host", "testdata/RSAClientCert.p12", "Path to Client Cert bundle in p12 format")
	serveCmd.Flags().StringVar(&clientCert, "client-cert", "testdata/RSAClientCert.p12", "Path to Client Cert bundle in p12 format")
	serveCmd.Flags().StringVar(&clientCertPassword, "client-cert-pw", "", "Password for password protected p12 files.")
	serveCmd.Flags().StringVar(&dbDialect, "db-dialect", "sqlite3", "Gorm supported SQL database dialect")
	serveCmd.Flags().StringVar(&dbConn, "db-conn", "file::memory:?cache=shared", "SQL database connection")
	serveCmd.Flags().StringVar(&mysqlConfig.User, "mysql-username", "root", "MySQL Username")
	serveCmd.Flags().StringVar(&mysqlConfig.Passwd, "mysql-password", "", "MySQL Password")
	serveCmd.Flags().StringVar(&mysqlConfig.Addr, "mysql-address", "slingshot-db:3306", "MySQL Address as host:port ")
	serveCmd.Flags().StringVar(&mysqlConfig.DBName, "mysql-dbname", "slingshot", "MySQL Password")
	serveCmd.Flags().BoolVar(&mysqlConfig.ParseTime, "parse-time", true, "MySQL ParseTime")

}

func listenAndgrpcServe() (err error) {

	lis, err := net.Listen("tcp", "localhost:9090")
	if err != nil {
		fmt.Printf("failed to listen: %v", err)
	}
	secondIntercerptor := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		fmt.Printf("Path: %s second interceptor called, It will only be called for input parameters are correct \n", info.FullMethod)
		//Get all chained interceptors
		return handler(ctx, req)
	}
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
			fmt.Printf("Path: %s first interceptor called \n", info.FullMethod)
			//Get all chained interceptors
			return handler(ctx, req)
		}),
		//grpc.UnaryInterceptor(grpc_validation.UnaryServerInterceptor()),
		grpc.ChainUnaryInterceptor(grpc_validation.UnaryServerInterceptor(), secondIntercerptor),
	}

	grpcServer := grpc.NewServer(serverOptions...)
	sbServer = &cpserver.Server{}
	cp.RegisterCloudProjectServer(grpcServer, sbServer)
	fmt.Printf("\nStarted grpc server on address localhost:9090")
	if err = grpcServer.Serve(lis); err != nil {
		fmt.Errorf("Grpc serve failed Error %s ", err.Error())
		return
	}

	return
}
func listenAndServe(addr string) (err error) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	grpcServerEndpoint := flag.String("grpc-server-endpoint", "localhost:9090", "gRPC server endpoint")

	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err = cp.RegisterCloudProjectHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)
	if err != nil {
		return err
	}

	// Start HTTP server (and proxy calls to gRPC server endpoint)

	fmt.Printf("Started http server on address %s ...", addr)
	return http.ListenAndServe(addr, mux)
}

func serve() (err error) {
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
	port = defaultPort
	env := os.Getenv("PORT")
	if env != "" {
		port, _ = strconv.Atoi(env)
		fmt.Printf("using port : %s \n", env)
	}

	addr := fmt.Sprintf("%s:%d", host, port)
	go listenAndgrpcServe()
	if err = listenAndServe(addr); err != nil {
		fmt.Printf("Error in listen exit ")
		os.Exit(-1)
	}

	return
}

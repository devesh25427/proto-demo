// +build tools

package cloudproject

import (
	_ "github.com/infobloxopen/protoc-gen-gorm/types"
	_ "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options"
	_ "github.com/golang/protobuf/protoc-gen-go"
	_ "github.com/infobloxopen/protoc-gen-gorm/options"
)

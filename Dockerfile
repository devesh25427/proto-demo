FROM golang:1.14 as builder
RUN apt-get update ; apt-get install sqlite3 libsqlite3-dev libc6 -y ; rm -rf /var/lib/apt/lists/
ADD apis /cloudproject/apis
ADD go.mod /cloudproject/go.mod
ADD pkg /cloudproject/pkg
ADD cmd/cp /cloudproject/cmd/cp
ADD vendor /cloudproject/vendor
WORKDIR /cloudproject/
ENV GOOS linux
ENV GOARCH amd64
# TODO: set back once sqlite3 removed as code dep(postgres doesn't need CGO)
ENV CGO_ENABLED=1
RUN go build -o cp cmd/cp/main.go

# TODO: set back to distroless once we rmoeve sqlite3
#FROM gcr.io/distroless/static as run
FROM golang:1.14 as run
COPY --from=builder  /cloudproject/cp /cp

ENTRYPOINT ["/cp", "serve"]
#ENTRYPOINT ["bash"]

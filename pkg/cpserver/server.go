package cpserver

import (
	"context"
	"crypto"
	"crypto/x509"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/devesh25427/cloudproject/apis/cp/v2"
)

//Server : Server struct
type Server struct {
	clientCert *x509.Certificate
	clientKey  crypto.Signer
}

var infolist []cp.Info

//InfoGet(context.Context, *InfoGetRequest) (*Info, error)
// put : update
//	InfoPut(context.Context, *Info) (*InfoPutResponse, error)

//InfoGet :
func (s Server) InfoGet(ctx context.Context, request *cp.InfoGetRequest) (resp *cp.Info, err error) {
	fmt.Printf("ENTER InfoGet Input id is %s\n", request.InfoId)
	resp = &cp.Info{}
	for _, l := range infolist {
		if l.InfoId == request.InfoId {
			resp.Value = l.Value
			resp.InfoId = l.InfoId
			resp.Description = l.Description
			fmt.Printf("Return InfoGet : found %v\n", resp)
			return
		}
	}
	fmt.Printf("Return InfoGet : Not found \n")
	return
}

func (s Server) InfoPut(ctx context.Context, request *cp.Info) (resp *cp.InfoPutResponse, err error) {
	logrus.Infof("ENTER InfoPut \n")
	resp = &cp.InfoPutResponse{}
	for _, l := range infolist {
		if l.InfoId == request.InfoId {
			fmt.Printf("InfoPut : deplicate exist\n")
			return
		}
	}
	var r cp.Info
	r.Value = request.Body.Value
	r.InfoId = request.Body.InfoId
	r.Description = request.Body.Description
	infolist = append(infolist, r)
	fmt.Printf("InfoPut \n")
	return
}
